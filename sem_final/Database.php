<?php


class Database
{
    private string $db_host = 'localhost';
    private string $db_name = 'iwww';
    private string $db_username = 'koral';
    private string $db_password = 'koral';


    public function dbConnection(){

        try{
            $conn = new PDO('mysql:host='.$this->db_host.';dbname='.$this->db_name,$this->db_username,$this->db_password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        }
        catch(PDOException $e){
            echo "Connection errorZZZ ".$e->getMessage();
            exit;
        }


    }
}