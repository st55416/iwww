<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// CHECK GET ID PARAMETER OR NOT
if (isset($_GET['id'])) {
    //IF HAS ID PARAMETER
    $row_id = filter_var($_GET['id'], FILTER_VALIDATE_INT, [
        'options' => [
            'default' => 'all_posts',
            'min_range' => 1
        ]
    ]);
} else {
    $row_id = 'all_posts';
}

if (isset($_GET['t'])) {
    $table_name = $_GET['t'];
} else {
    echo json_encode(['message' => 'No table specified']);
    return;
}

// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS
$sql = is_numeric($row_id) ? "SELECT * FROM `$table_name` WHERE id='$row_id'" : "SELECT * FROM `$table_name`";

$stmt = $conn->prepare($sql);

$stmt->execute();

// CREATE POSTS ARRAY
$posts_array = [];

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

    // PUSH POST DATA IN OUR $posts_array ARRAY
    array_push($posts_array, $row);
}
//SHOW POST/POSTS IN JSON FORMAT
echo json_encode($posts_array);


