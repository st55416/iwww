<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: DELETE");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: TABLE, ROWID, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

if (!isset($_SERVER['HTTP_TABLE'])) {
    $msg['message'] = 'No table specified';
    echo json_encode($msg);
    return;
}
if (!isset($_SERVER['HTTP_ROWID'])) {
    $msg['message'] = 'No row id specified';
    echo json_encode($msg);
    return;
}

$table_name = $_SERVER['HTTP_TABLE'];
$row_id = $_SERVER['HTTP_ROWID'];


$delete_query = "DELETE FROM $table_name WHERE id = :id;";

$delete_stmt = $conn->prepare($delete_query);

$delete_stmt->bindValue(':id', htmlspecialchars(strip_tags($row_id)), PDO::PARAM_INT);

if ($delete_stmt->execute()) {
    $msg['message'] = 'Data deleted Successfully';
} else {
    $msg['message'] = 'Data not deleted';
}


//ECHO DATA IN JSON FORMAT
echo json_encode($msg);