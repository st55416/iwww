<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: TABLE, ROWID, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

if (!isset($_SERVER['HTTP_TABLE'])) {
    $msg['message'] = 'No table specified';
    echo json_encode($msg);
    return;
}
if (!isset($_SERVER['HTTP_ROWID'])) {
    $msg['message'] = 'No row id specified';
    echo json_encode($msg);
    return;
}

$table_name = $_SERVER['HTTP_TABLE'];
$row_id = $_SERVER['HTTP_ROWID'];

if ($table_name === 'artist') { // ---------------------------------ARTIST
    if (isset($data->name) ) {
        // CHECK DATA VALUE IS EMPTY OR NOT
        if (!empty($data->name) ) {

            $update_query = "UPDATE artist SET name = :name WHERE id = :id;";

            $update_stmt = $conn->prepare($update_query);
            // DATA BINDING
            $update_stmt->bindValue(':id', htmlspecialchars(strip_tags($row_id)), PDO::PARAM_INT);
            $update_stmt->bindValue(':name', htmlspecialchars(strip_tags($data->name)), PDO::PARAM_STR);

            if ($update_stmt->execute()) {
                $msg['message'] = 'Data updated Successfully';
            } else {
                $msg['message'] = 'Data not updated';
            }

        } else {
            $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
        }
    } else {
        $msg['message'] = 'Please fill all the fields |';
    }
} else if ($table_name === 'song') {// ---------------------------------SONG
    if (isset($data->artist_id) && isset($data->title)) {
        // CHECK DATA VALUE IS EMPTY OR NOT
        if (!empty($data->artist_id) && !empty($data->title)) {

            $update_query = "UPDATE song SET artist_id = :artist_id, title = :title WHERE id = :id;";

            $update_stmt = $conn->prepare($update_query);
            // DATA BINDING
            $update_stmt->bindValue(':id', htmlspecialchars(strip_tags($row_id)), PDO::PARAM_INT);
            $update_stmt->bindValue(':artist_id', htmlspecialchars(strip_tags($data->artist_id)), PDO::PARAM_INT);
            $update_stmt->bindValue(':title', htmlspecialchars(strip_tags($data->title)), PDO::PARAM_STR);

            if ($update_stmt->execute()) {
                $msg['message'] = 'Data updated Successfully';
            } else {
                $msg['message'] = 'Data not updated';
            }

        } else {
            $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
        }
    } else {
        $msg['message'] = 'Please fill all the fields |';
    }
} else {
    $msg['message'] = "Table $table_name not found";
}

//ECHO DATA IN JSON FORMAT
echo json_encode($msg);