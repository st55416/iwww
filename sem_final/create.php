<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: TABLE, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

if (!isset($_SERVER['HTTP_TABLE'])) {
    $msg['message'] = 'No table specified';
    echo json_encode($msg);
    return;
}

$table_name = $_SERVER['HTTP_TABLE'];

if ($table_name === 'artist') { // ---------------------------------ARTIST
    if (isset($data->name) ) {
        // CHECK DATA VALUE IS EMPTY OR NOT
        if (!empty($data->name) ) {

            $insert_query = "INSERT INTO `artist`(name) VALUES(:name)";

            $insert_stmt = $conn->prepare($insert_query);
            // DATA BINDING
            $insert_stmt->bindValue(':name', htmlspecialchars(strip_tags($data->name)), PDO::PARAM_STR);

            if ($insert_stmt->execute()) {
                $msg['message'] = 'Data Inserted Successfully';
            } else {
                $msg['message'] = 'Data not Inserted';
            }

        } else {
            $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
        }
    } else {
        $msg['message'] = 'Please fill all the fields |';
    }
} else if ($table_name === 'song') {// ---------------------------------SONG
    if (isset($data->artist_id) && isset($data->title)) {
        // CHECK DATA VALUE IS EMPTY OR NOT
        if (!empty($data->artist_id) && !empty($data->title) ) {

            $insert_query = "INSERT INTO `song`(artist_id,title) VALUES(:artist_id,:title)";

            $insert_stmt = $conn->prepare($insert_query);
            // DATA BINDING
            $insert_stmt->bindValue(':artist_id', htmlspecialchars(strip_tags($data->artist_id)), PDO::PARAM_INT);
            $insert_stmt->bindValue(':title', htmlspecialchars(strip_tags($data->title)), PDO::PARAM_STR);

            if ($insert_stmt->execute()) {
                $msg['message'] = 'Data Inserted Successfully';
            } else {
                $msg['message'] = 'Data not Inserted';
            }

        } else {
            $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
        }
    } else {
        $msg['message'] = 'Please fill all the fields |';
    }
} else {
    $msg['message'] = "Table $table_name not found";
}

//ECHO DATA IN JSON FORMAT
echo json_encode($msg);