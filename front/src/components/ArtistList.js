import React from 'react'
import Artist from './Artist'

class ArtistList extends React.Component {

  state = {
    input: ''
  }

  handleArtistInput = (event) => {
    this.setState({
      input: event.target.value
    })
  }

  render() {
    var artistsList = this.props.artistsArray.map(artist => {
      return <Artist updateArtist={this.props.updateArtist} artist={artist} key={artist.id} updateSongsByArtist={this.props.updateSongsByArtist} removeArtist={this.props.removeArtist} activeId={this.props.activeId} />
    })
    return (
      <div className="to-do-card">
        <h4>Artists</h4>
        <form onSubmit={(event) => {
          this.setState({
            input: ''
          })
          this.props.postArtist(event, this.state.input)
        }}>
          <input onChange={this.handleArtistInput} type="text" value={this.state.input} />
        </form>
        {artistsList}
      </div>
    )
  }
}


export default ArtistList
