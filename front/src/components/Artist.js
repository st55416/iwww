import React from 'react'
import MainContainer from '../containers/MainContainer'
import SongList from './SongList'

//const Artist = (props) => {
class Artist extends React.Component {

  state = {
    editing: false,
    input: this.props.artist.name
  }

  handleArtistInput = (event) => {
    this.setState({
      input: event.target.value
    })
  }

  render() {
    let content;
    let entireBlock;
    let editStyle = "edit ";
    if (this.state.editing) {
      content = <form onSubmit={(event) => {
        event.preventDefault()
        this.setState({
          editing: false
        })
        this.props.updateArtist(this.props.artist.id, this.state.input)
      }}>
        <input className="edit-field" onChange={this.handleArtistInput} type="text" value={this.state.input} />
      </form>
    } else {
      
      if (this.props.activeId == this.props.artist.id) {
        editStyle += "active";
      }
      content = this.props.activeId == this.props.artist.id ?
        <h3 className="to-do-list active">{this.props.artist.name}</h3> :
        <h3 className="to-do-list">{this.props.artist.name}</h3>
    }
    entireBlock = <div className="to-do-list-container animated" onClick={(event) => this.props.updateSongsByArtist(this.props.artist.id)}>
      <span className="rem-btn" onClick={() => this.props.removeArtist(this.props.artist.id)}>+</span>
      <a className={editStyle} onClick={() => this.setState({ editing: !this.state.editing })}>Edit</a>
      {content}
    </div>

    return (
      <div>
        {entireBlock}
      </div>
    )
  }
}


export default Artist
