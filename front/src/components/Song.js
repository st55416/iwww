import React from 'react'

class Song extends React.Component {

  state = {
    editing: false,
    input: this.props.song.title
  }

  handleSongInput = (event) => {
    this.setState({
      input: event.target.value
    })
  }


  render() {
    let content;

    if (this.state.editing) {
      content = <form onSubmit={(event) => {
        event.preventDefault()
        this.setState({
          editing: false
        })
        this.props.updateSong(this.props.song.id, this.state.input, this.props.song.artist_id)
      }}>
        <input onChange={this.handleSongInput} type="text" value={this.state.input} />
      </form>
    } else {
      content = <h3 className="to-do-list">{this.props.song.title}</h3>
    }

    return (
      <div className="to-do-list-container animated">
        <span className="rem-btn" onClick={() => this.props.removeSong(this.props.song.id)}>+</span>
        <a className="edit song-edit" onClick={() => this.setState({ editing: !this.state.editing })}>Edit</a>
        {content}
      </div>
    )
  }
}

export default Song
