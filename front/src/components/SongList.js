import React from 'react'
import Song from './Song'

class SongList extends React.Component {

  state = {
    input: ''
  }

  handleSongInput = (event) => {
    this.setState({
      input: event.target.value
    })
  }

  render() {
    const songsList = this.props.songsArray.map(song => {
      return <Song song={song} key={song.id} removeSong={this.props.removeSong} updateSong={this.props.updateSong}/>
    })

    return (
      <div className="to-do-card">
        <h4>Songs by "{this.props.artName}"</h4>
        <form onSubmit={(event) => {
          event.preventDefault()
          this.setState({
            input: ''
          })
          this.props.postSong(this.props.artistId, this.state.input)
        }}>
          <input onChange={this.handleSongInput} type="text" value={this.state.input} />
        </form>
        {songsList}
      </div>
    )
  }
}


export default SongList
