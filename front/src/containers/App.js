import React from 'react';
import '../App.css';
import MainContainer from './MainContainer'


function App() {
  return (
    <div className="App">
      <h1>Music Library by Lupenko Andrii</h1>
     <MainContainer />
    </div>
  );
}

export default App;
