import React from 'react'
import SongList from '../components/SongList'
import ArtistList from '../components/ArtistList'


export default class MainContainer extends React.Component {

  constructor(props) {
    super(props)

    this.updateSongsByArtist = this.updateSongsByArtist.bind(this)
    this.postSong = this.postSong.bind(this)
    this.postArtist = this.postArtist.bind(this)
    this.removeArtist = this.removeArtist.bind(this)
    this.removeSong = this.removeSong.bind(this)
    this.loadFromDB = this.loadFromDB.bind(this)
    this.updateArtist = this.updateArtist.bind(this)
    this.updateSong = this.updateSong.bind(this)

  }

  state = {
    songs: [],
    currentSongs: [],
    artists: [],
    currentArtistId: undefined
  }

  componentDidMount() {
    this.loadFromDB();
  }

  loadFromDB() {
    fetch("http://localhost:8000/read.php?t=song")
      .then(resp => resp.json())
      .then(newSongs => {
        this.setState({
          songs: newSongs
        })
      })
    fetch("http://localhost:8000/read.php?t=artist")
      .then(resp => resp.json())
      .then(newArtists => {
        this.setState({
          artists: newArtists
        })
      })
  }

  updateSongsByArtist(artistId) {
    this.setState({
      currentArtistId: artistId
    })
  }

  updateArtist(artistId, name) {
    fetch("http://localhost:8000/update.php", {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        'TABLE': 'artist',
        'ROWID': artistId
      },
      body: JSON.stringify({
        "name": name
      })
    }).then((response) => response.text())
      .then((resp) => {
        this.loadFromDB()
      })
  }


  updateSong(songId, name, artistId) {
    fetch("http://localhost:8000/update.php", {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        'TABLE': 'song',
        'ROWID': songId
      },
      body: JSON.stringify({
        "artist_id": artistId,
        "title": name
      })
    }).then((response) => response.text())
      .then((resp) => {
        this.loadFromDB()
      })
  }

  postSong(artistId, title) {

    fetch("http://localhost:8000/create.php", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'TABLE': 'song'
      },
      body: JSON.stringify({
        "artist_id": artistId,
        "title": title
      })
    }).then((response) => response.text())
      .then((resp) => {
        this.loadFromDB()
      })
  }

  postArtist(event, name) {
    event.preventDefault()

    fetch("http://localhost:8000/create.php", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'TABLE': 'artist'
      },
      body: JSON.stringify({
        "name": name
      })
    }).then((response) => response.text())
      .then((resp) => {
        this.loadFromDB();
      })
  }

  removeSong(id) {
    fetch("http://localhost:8000/delete.php", {
      method: "DELETE",
      headers: {
        'TABLE': 'song',
        'ROWID': id
      }
    }).then((response) => response.text())
      .then((resp) => this.loadFromDB())
  }

  removeArtist(id) {
    fetch("http://localhost:8000/delete.php", {
      method: "DELETE",
      headers: {
        'TABLE': 'artist',
        'ROWID': id
      }
    }).then((response) => response.text())
      .then((resp) => this.loadFromDB())
  }

  render() {
    const currArt = this.state.artists.filter((art) => art.id === this.state.currentArtistId);
    const artName = currArt.length === 0 ? "CLICK ON ARTIST" : currArt[0].name;

    const updatedSongs = this.state.songs.filter((song) => song.artist_id === this.state.currentArtistId)

    return (
      <div className="main-container">
        <div>
          <ArtistList artistsArray={this.state.artists} updateSongsByArtist={this.updateSongsByArtist}
            postArtist={this.postArtist} removeArtist={this.removeArtist} updateArtist={this.updateArtist}
            activeId={this.state.currentArtistId} />
          <SongList songsArray={updatedSongs} artistId={this.state.currentArtistId}
            artName={artName} postSong={this.postSong} removeSong={this.removeSong} updateSong={this.updateSong} />
        </div>
      </div>
    )
  }



}
